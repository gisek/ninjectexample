using NinjectExample.Attributes;
using NinjectExample.PaymentMethods;

namespace NinjectExample.Shoppers
{
    [CreditCardNeeded]
    public class CreditCardShopper
    {
        private readonly IPaymentMethod _paymentMethod;

        public CreditCardShopper(IPaymentMethod paymentMethod)
        {
            _paymentMethod = paymentMethod;
        }

        public void Charge()
        {
            _paymentMethod.Charge();
        }
    }
}