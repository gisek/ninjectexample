using Ninject;
using NinjectExample.PaymentMethods;

namespace NinjectExample.Shoppers
{
    public class ShopperForPropertyInjection
    {
        [Inject]
        public IPaymentMethod PaymentMethod { get; set; }

        public void Charge()
        {
            PaymentMethod.Charge();
        }
    }
}