using NinjectExample.Attributes;
using NinjectExample.PaymentMethods;

namespace NinjectExample.Shoppers
{
    [TransferNeeded]
    public class TransferShopper
    {
        private readonly IPaymentMethod _paymentMethod;

        public TransferShopper(IPaymentMethod paymentMethod)
        {
            _paymentMethod = paymentMethod;
        }

        public void Charge()
        {
            _paymentMethod.Charge();
        }
    }
}