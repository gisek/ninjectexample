using NinjectExample.PaymentMethods;

namespace NinjectExample.Shoppers
{
    public class Shopper
    {
        private readonly IPaymentMethod _paymentMethod;

        public Shopper(IPaymentMethod paymentMethod)
        {
            _paymentMethod = paymentMethod;
        }

        public void Charge()
        {
            _paymentMethod.Charge();
        }
    }
}