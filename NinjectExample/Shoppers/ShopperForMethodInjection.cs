using Ninject;
using NinjectExample.PaymentMethods;

namespace NinjectExample.Shoppers
{
    public class ShopperForMethodInjection
    {
        private readonly IPaymentMethod _paymentMethod;

        public ShopperForMethodInjection(IPaymentMethod paymentMethod)
        {
            _paymentMethod = paymentMethod;
        }

        [Inject]
        public void Charge()
        {
            _paymentMethod.Charge();
        }
    }
}