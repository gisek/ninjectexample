using System;

namespace NinjectExample.Dummy
{
    public class DummyClass
    {
        public string Name { set; get; }

        public DummyClass()
        {
            Name = "Default name";
        }

        public void Introduce()
        {
            Console.WriteLine(string.Format("I'm a concrete class, you don't have to register me to get my instance from kernel. My name is '{0}'", Name));
        }
    }
}