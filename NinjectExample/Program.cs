﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using NinjectExample.Attributes;
using NinjectExample.Dummy;
using NinjectExample.PaymentMethods;
using NinjectExample.Shoppers;

namespace NinjectExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var rand = new Random();

            var kernel = new StandardKernel();
            
            #region concrete class
            
            Console.WriteLine("---> Concrete class");

            var dummyObj = kernel.Get<DummyClass>(); // get instance of a concrete class withot registering
            dummyObj.Introduce();
            
            Console.ReadLine();

            #endregion

            #region simple binding

            Console.WriteLine("---> Simple binding");
            kernel.Rebind<IPaymentMethod>().To<Visa>(); //simple binging
            kernel.Get<IPaymentMethod>().Charge(); // get bound object

            Console.ReadLine();

            #endregion

            #region new instances from Get and using Rebind

            Console.WriteLine("---> New instances from Get, using Rebind");
            for (int i = 0; i < 4; i++)
            {
                if (i % 2 == 0)
                {
                    // call Redind only after Bind was called, otherwise it won't work with conditional binding
                    // Rebind allows to change configuration multipe times
                    kernel.Rebind<IPaymentMethod>().To<Visa>(); 
                }
                else
                {
                    kernel.Rebind<IPaymentMethod>().To<MasterCard>();
                }

                var shopper = new Shopper(kernel.Get<IPaymentMethod>());
                shopper.Charge();
            }

            Console.ReadLine();

            #endregion

            #region singletons from Get

            Console.WriteLine("---> Singletons from Get");
            kernel.Rebind<IPaymentMethod>().To<Maestro>().InSingletonScope(); // bind class in singleton mode
            //kernel.Rebind<IPaymentMethod>().To<Maestro>().InThreadScope(); // bind class in singleton mode - one singleton per Thread
            //some other modes availabel e.q. per web request

            for (int i = 0; i < 3; i++)
            {
                var shopper = new Shopper(kernel.Get<IPaymentMethod>());
                shopper.Charge();
            }

            Console.ReadLine();

            #endregion

            #region Cusomt object initialization

            Console.WriteLine("---> Cusomt object initialization");
            kernel.Bind<DummyClass>().ToMethod(context =>
            {
                // you can do whatever you want here. E.q. setup properties
                var dummy = new DummyClass();
                dummy.Name = "---->Custom name<----";
                return dummy;
            });

            var cusotmDummyObj = kernel.Get<DummyClass>();
            cusotmDummyObj.Introduce();

            Console.ReadLine();

            #endregion

            #region contextual binding

            Console.WriteLine("---> Contextual binding");
            Console.WriteLine("--- ---> Per dependant class type");
            kernel.Bind<IPaymentMethod>().To<Visa>().WhenInjectedInto<CreditCardShopper>();
            kernel.Bind<IPaymentMethod>().To<AliorBankTransfer>().WhenInjectedInto<TransferShopper>();

            //this way won't work
            //var creditCardShopper = new CreditCardShopper(kernel.Get<IPaymentMethod>());
            //var transferShopper = new TransferShopper(kernel.Get<IPaymentMethod>());

            //this way will work
            var creditCardShopper = kernel.Get<CreditCardShopper>();
            var transferShopper = kernel.Get<TransferShopper>();

            creditCardShopper.Charge();
            transferShopper.Charge();

            kernel.Unbind<IPaymentMethod>();

            Console.ReadLine();

            Console.WriteLine("--- ---> Per dependant class attribute");
            kernel.Bind<IPaymentMethod>().To<MasterCard>().WhenClassHas<CreditCardNeeded>();
            kernel.Bind<IPaymentMethod>().To<MBankTransfer>().WhenClassHas<TransferNeeded>();

            var creditCardNeededShopper = kernel.Get<CreditCardShopper>();
            var transferneededShopper = kernel.Get<TransferShopper>();

            creditCardNeededShopper.Charge();
            transferneededShopper.Charge();

            Console.ReadLine();

            #endregion

            #region property injection

            Console.WriteLine("---> Property injection");

            kernel.Bind<IPaymentMethod>().To<Visa>();

            // injects property with [Inject] attribute automatically after initialization
            // work the same way with fields
            var shopperForPropertyInjection = kernel.Get<ShopperForPropertyInjection>();
            shopperForPropertyInjection.Charge();

            Console.ReadLine();

            #endregion

            #region method injection

            Console.WriteLine("---> Method injection");

            // method with [Inject] attribute will be automatically called after initialization
            var shopperForMethodInjection = kernel.Get<ShopperForMethodInjection>();

            Console.ReadLine();

            #endregion

            Console.WriteLine("Done, press <Enter> to exit.");
            Console.ReadLine();
        }
    }
}
