using System;

namespace NinjectExample.PaymentMethods
{
    public class AliorBankTransfer : IPaymentMethod
    {
        private int _timesCharged;

        public void RegisterPayment()
        {
            _timesCharged += 1;
        }

        public void Charge()
        {
            RegisterPayment();
            Console.WriteLine(string.Format("Charging with Alior Bank transfer. Charges by this object: {0}", _timesCharged));
        }
    }
}