namespace NinjectExample.PaymentMethods
{
    public interface IPaymentMethod
    {
        void RegisterPayment();
        void Charge();
    }
}